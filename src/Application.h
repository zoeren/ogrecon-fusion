#ifndef _Application_h_
#define _Application_h_

#include "stdafx.h"
#include "AppStateManager.h"

typedef std::map<Ogre::String, AppState*> AppStatesMap;

class Application
{
public:
	Application();
	~Application();

	void run(AppStatesMap states, AppState* initialState);

private:
	AppStateManager* _stateManager;
};

#endif