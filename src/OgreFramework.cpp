#include "OgreFramework.h"

template<> OgreFramework* Ogre::Singleton<OgreFramework>::msSingleton = 0;

OgreFramework::OgreFramework() :
	root(0),
	renderWindow(0),
	overlaySystem(0),
	viewport(0),
	timer(0),
	log(0),
	inputManager(0),
	keyboard(0),
	mouse(0),
	guiRenderer(0),
	guiSystem(0),
	debugOverlay(0)
{
}

OgreFramework::~OgreFramework()
{
	if(inputManager) OIS::InputManager::destroyInputSystem(inputManager);
	if(root) delete root;
}

bool OgreFramework::start(Ogre::String windowTitle)
{
	new OgreFramework();
	return OgreFramework::getSingletonPtr()->initialise(windowTitle);
}

bool OgreFramework::initialise(Ogre::String wndTitle)
{
	Ogre::LogManager* logManager = new Ogre::LogManager();
	log = logManager->createLog("Durak.log", true, true);
		
#ifdef _DEBUG
	_configResources = "resources_d.cfg";
	_configPlugins = "plugins_d.cfg";
#else
    _configResources = "resources.cfg";
    _configPlugins = "plugins.cfg";
#endif

	root = new Ogre::Root(_configPlugins);
	if (!root->restoreConfig())
		if(!root->showConfigDialog())
			return false;
	root->addFrameListener(this);
	
	renderWindow = root->initialise(true, wndTitle);

    viewport = renderWindow->addViewport(0);
	viewport->setBackgroundColour(Ogre::ColourValue(0.0f, 0.0f, 0.0f));
    viewport->setCamera(0);
	
	overlaySystem = new Ogre::OverlaySystem;

	initialiseInput();
	initialiseResources();
	initialiseUserInterface();
	initialiseDebugStats();

	//Set initial mouse clipping size
	//windowResized(renderWindow);
	
	//Register as a Window listener
    //Ogre::WindowEventUtilities::addWindowEventListener(renderWindow, this);

	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

	this->timer = new Ogre::Timer;
	this->timer->reset();
	
	renderWindow->setActive(true);

	return true;
}

void OgreFramework::initialiseResources()
{
    Ogre::ConfigFile cf;
	cf.load(_configResources);

    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
	Ogre::String secName, typeName, archName;
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }
    }
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void OgreFramework::initialiseInput()
{
	size_t hWnd = 0;
    OIS::ParamList paramList;
	renderWindow->getCustomAttribute("WINDOW", &hWnd);

    paramList.insert(OIS::ParamList::value_type("WINDOW", Ogre::StringConverter::toString(hWnd)));

	inputManager = OIS::InputManager::createInputSystem(paramList);

	keyboard = static_cast<OIS::Keyboard*>(inputManager->createInputObject(OIS::OISKeyboard, true));
    mouse = static_cast<OIS::Mouse*>(inputManager->createInputObject(OIS::OISMouse, true));

    mouse->getMouseState().height = renderWindow->getHeight();
    mouse->getMouseState().width = renderWindow->getWidth();

    keyboard->setEventCallback(this);
    mouse->setEventCallback(this);
}

void OgreFramework::initialiseUserInterface()
{
	guiRenderer = &CEGUI::OgreRenderer::bootstrapSystem();
	guiSystem = CEGUI::System::getSingletonPtr();

	//Set default resource groups for each CEGUI's resource manager
	CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
	CEGUI::Font::setDefaultResourceGroup("Fonts");
	CEGUI::Scheme::setDefaultResourceGroup("Schemes");
	CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
	CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
 
	//Set the skin/scheme
	CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
 
	//Set default mouse cursor
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");
}

void OgreFramework::update(Ogre::Real timeSinceLastFrame)
{
	updateStats();
}

bool OgreFramework::keyPressed(const OIS::KeyEvent &keyEventRef)
{
	/*
	if(keyboard->isKeyDown(OIS::KC_D))
    {
		if(_debugStatsOverlay->isVisible())
            _debugStatsOverlay->hide();
        else
            _debugStatsOverlay->show();
    }
	*/
	return true;
}

bool OgreFramework::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	return true;
}

bool OgreFramework::mouseMoved(const OIS::MouseEvent &evt)
{
    return true;
}

bool OgreFramework::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}

bool OgreFramework::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}

bool OgreFramework::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	if(renderWindow->isClosed())
        return false;
 
    //Need to capture/update each device
    keyboard->capture();
    mouse->capture();
 
    //Need to inject timestamps to CEGUI System.
    CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);
 
	return true;
}

void OgreFramework::initialiseDebugStats()
{
	debugOverlay = Ogre::OverlayManager::getSingletonPtr()->getByName("Core/DebugOverlay");
	debugOverlay->show();
}

void OgreFramework::updateStats()
{ 
	static Ogre::String currFps = "Current FPS: "; 
	static Ogre::String avgFps = "Average FPS: "; 
	static Ogre::String bestFps = "Best FPS: "; 
	static Ogre::String worstFps = "Worst FPS: "; 
	static Ogre::String tris = "Triangle Count: "; 
	static Ogre::String batches = "Batch Count: "; 

	Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::OverlayElement* guiAvg = overlayManager->getOverlayElement("Core/AverageFps"); 
	Ogre::OverlayElement* guiCurr = overlayManager->getOverlayElement("Core/CurrFps"); 
	Ogre::OverlayElement* guiBest = overlayManager->getOverlayElement("Core/BestFps"); 
	Ogre::OverlayElement* guiWorst = overlayManager->getOverlayElement("Core/WorstFps"); 

	const Ogre::RenderTarget::FrameStats& stats = renderWindow->getStatistics(); 
	guiAvg->setCaption(avgFps + Ogre::StringConverter::toString(stats.avgFPS)); 
	guiCurr->setCaption(currFps + Ogre::StringConverter::toString(stats.lastFPS)); 
	guiBest->setCaption(bestFps + Ogre::StringConverter::toString(stats.bestFPS) 
		+" "+Ogre::StringConverter::toString(stats.bestFrameTime)+" ms"); 
	guiWorst->setCaption(worstFps + Ogre::StringConverter::toString(stats.worstFPS) 
		+" "+Ogre::StringConverter::toString(stats.worstFrameTime)+" ms"); 

	Ogre::OverlayElement* guiTris = overlayManager->getOverlayElement("Core/NumTris"); 
	guiTris->setCaption(tris + Ogre::StringConverter::toString(stats.triangleCount)); 

	Ogre::OverlayElement* guiBatches = overlayManager->getOverlayElement("Core/NumBatches"); 
	guiBatches->setCaption(batches + Ogre::StringConverter::toString(stats.batchCount)); 

	Ogre::OverlayElement* guiDbg = overlayManager->getOverlayElement("Core/DebugText"); 
	guiDbg->setCaption("");
}