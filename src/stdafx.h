#include <vector>
#include <map>

#include <OGRE/OgreCamera.h>
#include <OGRE/OgreConfigFile.h>
#include <OGRE/OgreException.h>
#include <OGRE/OgreEntity.h>
#include <OGRE/OgreFrameListener.h>
#include <OGRE/OgreLogManager.h>
#include <OGRE/OgreRenderWindow.h>
#include <OGRE/OgreRoot.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreSingleton.h>
#include <OGRE/OgreTimer.h>
#include <OGRE/OgreViewport.h>
#include <OGRE/OgreWindowEventUtilities.h>
#include <OGRE/Overlay/OgreOverlaySystem.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif
