#include "AppStateManager.h"


AppStateManager::AppStateManager() :
	_shutdown(false)
{

}

AppStateManager::~AppStateManager()
{

}

void AppStateManager::registerState(AppState* state, Ogre::String stateName)
{
	if (state->_parent == 0)
	{
		state->_parent = this;
	}
	_registeredStates[stateName] = state;
}

void AppStateManager::start(AppState* state)
{
	changeState(state);

	Ogre::Real timeSinceLastFrame = 1;
	Ogre::Real startTime = 0;

	while (!_shutdown)
	{
		if(OgreFramework::getSingletonPtr()->renderWindow->isClosed())
			_shutdown = true;

		Ogre::WindowEventUtilities::messagePump();

		if(OgreFramework::getSingletonPtr()->renderWindow->isActive())
		{
			startTime = (Ogre::Real)OgreFramework::getSingletonPtr()->timer->getMillisecondsCPU();

			OgreFramework::getSingletonPtr()->keyboard->capture();
			OgreFramework::getSingletonPtr()->mouse->capture();

			_activeStates.back()->update(timeSinceLastFrame);

			OgreFramework::getSingletonPtr()->update(timeSinceLastFrame);
			OgreFramework::getSingletonPtr()->root->renderOneFrame();

			timeSinceLastFrame = OgreFramework::getSingletonPtr()->timer->getMillisecondsCPU() - startTime;
		}
		else
		{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			Sleep(1000);
#else
			sleep(1);
#endif
		}
	}
}

AppState* AppStateManager::findByName(Ogre::String stateName)
{
	return _registeredStates[stateName];
}

void AppStateManager::changeState(AppState* state)
{
	if (!_activeStates.empty())
	{
		_activeStates.back()->leave();
		_activeStates.pop_back();
	}
	_activeStates.push_back(state);
	initialiseState(state);
	_activeStates.back()->enter();
}

bool AppStateManager::pushState(AppState* state)
{
	if (!_activeStates.empty())
	{
		if (_activeStates.size() > 1)
			OgreFramework::getSingletonPtr()->log->logMessage("pushState is not implemented for > 1 states.", Ogre::LML_CRITICAL);
		else
		{
			_activeStates.back()->pause();
		}
	}

	_activeStates.push_back(state);
	initialiseState(state);
	_activeStates.back()->enter();

	return true;
}

void AppStateManager::popState()
{
	if (!_activeStates.empty())
	{
		_activeStates.back()->leave();
		_activeStates.pop_back();
	}

	if (!_activeStates.empty())
	{
		initialiseState(_activeStates.back());
		_activeStates.back()->resume();
	}
}

void AppStateManager::popAllAndPushState(AppState* state)
{
	while (!_activeStates.empty())
	{
		_activeStates.back()->leave();
		_activeStates.pop_back();
	}

	pushState(state);
}

void AppStateManager::pauseState()
{
	if (!_activeStates.empty())
	{
		_activeStates.back()->pause();
	}

	if (_activeStates.size() > 2)
	{
		AppState *state = _activeStates.at(_activeStates.size() - 2);
		initialiseState(state);
		state->resume();
	}
}

void AppStateManager::shutdown()
{
	_shutdown = true;
}

void AppStateManager::initialiseState(AppState* state)
{
	OgreFramework::getSingletonPtr()->keyboard->setEventCallback(state);
	OgreFramework::getSingletonPtr()->mouse->setEventCallback(state);
	OgreFramework::getSingletonPtr()->renderWindow->resetStatistics();
}
