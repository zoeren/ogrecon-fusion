#include "GamePauseState.h"


GamePauseState::GamePauseState()
{
	
}

GamePauseState::~GamePauseState()
{
}

void GamePauseState::createScene()
{

}

void GamePauseState::enter()
{
	OgreFramework::getSingletonPtr()->log->logMessage("GamePause: Enter");

	CEGUI::WindowManager &wmgr = CEGUI::WindowManager::getSingleton();
	_sheet = wmgr.createWindow("DefaultWindow", "GamePauseSheet");

	CEGUI::FrameWindow *frame = (CEGUI::FrameWindow*)wmgr.createWindow("TaharezLook/FrameWindow", "GamePauseFrame");
	frame->setCloseButtonEnabled(false);
	frame->setDragMovingEnabled(false);
	frame->setText("Pause");
	frame->setTitleBarEnabled(true);
	frame->setRollupEnabled(false);
	frame->setSizingEnabled(false);
	frame->setSize(CEGUI::USize(CEGUI::UDim(0.4f, 0.0f), CEGUI::UDim(0.4f, 0.0f)));
	frame->setPosition(CEGUI::UVector2(CEGUI::UDim(0.3f, 0.0f), CEGUI::UDim(0.3f, 0.0f)));

	// main menu
	CEGUI::USize defaultBtnSize = CEGUI::USize(CEGUI::UDim(0.7f, 0.0f), CEGUI::UDim(0.2f, 0.0f));

	CEGUI::PushButton *resume = (CEGUI::PushButton*)wmgr.createWindow("TaharezLook/Button", "ResumeButton");
	resume->setText("Resume");
	resume->setSize(defaultBtnSize);
	resume->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15f, 0.0f), CEGUI::UDim(0.1f, 0.0f)));
	resume->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GamePauseState::onResumeSelected, this));

	CEGUI::PushButton *mainMenu = (CEGUI::PushButton*)wmgr.createWindow("TaharezLook/Button", "MainMenuButton");
	mainMenu->setText("Main Menu");
	mainMenu->setSize(defaultBtnSize);
	mainMenu->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15f, 0.0f), CEGUI::UDim(0.4f, 0.0f)));
	mainMenu->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GamePauseState::onMainMenuSelected, this));

	CEGUI::PushButton *quit = (CEGUI::PushButton*)wmgr.createWindow("TaharezLook/Button", "QuitButton");
	quit->setText("Quit");
	quit->setSize(defaultBtnSize);
	quit->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15f, 0.0f), CEGUI::UDim(0.7f, 0.0f)));
	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GamePauseState::onQuitSelected, this));

	frame->addChild(resume);
	frame->addChild(mainMenu);
	frame->addChild(quit);

	_sheet->addChild(frame);
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_sheet);
}

void GamePauseState::leave()
{
	OgreFramework::getSingletonPtr()->log->logMessage("GamePause: Leave");

	CEGUI::WindowManager::getSingleton().destroyWindow(_sheet);
}

bool GamePauseState::keyPressed(const OIS::KeyEvent &evt)
{
	if (evt.key == OIS::KC_ESCAPE)
	{
		onResumeSelected();
	}
	return true;
}

void GamePauseState::update(Ogre::Real timeSinceLastFrame)
{

}

bool GamePauseState::onResumeSelected(const CEGUI::EventArgs &evt)
{
	popState();
	return true;
}

bool GamePauseState::onMainMenuSelected(const CEGUI::EventArgs &evt)
{
	popAllAndPushState("MainMenu");
	return true;
}

bool GamePauseState::onQuitSelected(const CEGUI::EventArgs &evt)
{
	shutdown();
	return true;
}
