#include "GuiAppState.h"

bool GuiAppState::keyPressed(const OIS::KeyEvent &evt)
{
	CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
	context.injectKeyDown((CEGUI::Key::Scan)evt.key);
	context.injectChar((CEGUI::Key::Scan)evt.text);
	return true;
}

bool GuiAppState::keyReleased(const OIS::KeyEvent &evt)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp((CEGUI::Key::Scan)evt.key);
	return true;
}

bool GuiAppState::mouseMoved(const OIS::MouseEvent &evt)
{
	CEGUI::System &sys = CEGUI::System::getSingleton();
	sys.getDefaultGUIContext().injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
	// Scroll wheel.
	if (evt.state.Z.rel)
		sys.getDefaultGUIContext().injectMouseWheelChange(evt.state.Z.rel / 120.0f);
	return true;
}

bool GuiAppState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertButton(id));
	return true;
}

bool GuiAppState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertButton(id));
	return true;
}

void GuiAppState::createGuiScene(Ogre::String name)
{
	_sceneManager = OgreFramework::getSingletonPtr()->root->createSceneManager(Ogre::ST_GENERIC, name + "SceneMgr");
	_sceneManager->addRenderQueueListener(OgreFramework::getSingletonPtr()->overlaySystem);

	_camera = _sceneManager->createCamera(name + "Cam");
	_camera->setPosition(Ogre::Vector3(0, 25, -50));
	_camera->lookAt(Ogre::Vector3(0, 0, 0));
	_camera->setNearClipDistance(1);
	_camera->setAspectRatio(Ogre::Real(OgreFramework::getSingletonPtr()->viewport->getActualWidth()) / Ogre::Real(OgreFramework::getSingletonPtr()->viewport->getActualHeight()));
	OgreFramework::getSingletonPtr()->viewport->setCamera(_camera);
}

void GuiAppState::destroyGuiScene()
{
	_sceneManager->destroyCamera(_camera);
	if(_sceneManager)
		OgreFramework::getSingletonPtr()->root->destroySceneManager(_sceneManager);
}

CEGUI::MouseButton GuiAppState::convertButton(OIS::MouseButtonID buttonID)
{
	switch (buttonID)
	{
	case OIS::MB_Left:
		return CEGUI::LeftButton;
	case OIS::MB_Right:
		return CEGUI::RightButton;
	case OIS::MB_Middle:
		return CEGUI::MiddleButton;
	default:
		return CEGUI::LeftButton;
	}
}

