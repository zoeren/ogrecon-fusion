#ifndef _GameState_h_
#define _GameState_h_

#include "stdafx.h"

#include "GuiAppState.h"
#include "game/Game.h"

class GameState : public GuiAppState
{
public:
	GameState(void);
	~GameState(void);

	void enter();
	void leave();
	void pause();
	void resume();

	bool keyPressed(const OIS::KeyEvent &evt);
	
	bool mouseMoved(const OIS::MouseEvent &arg);

	void update(Ogre::Real timeSinceLastFrame);

private:
	void createScene();

	bool _paused;

	Game *_game;

	Ogre::RaySceneQuery *_raySceneQuery;
	Ogre::SceneNode *_planet;
};

#endif