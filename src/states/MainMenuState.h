#ifndef _MainMenuState_h_
#define _MainMenuState_h_

#include "stdafx.h"

#include "GuiAppState.h"

class MainMenuState : public GuiAppState
{
public:
	MainMenuState();
	~MainMenuState();

	void createScene();

	void enter();
	void leave();

	bool keyPressed(const OIS::KeyEvent &evt);
	//bool keyReleased(const OIS::KeyEvent &evt);

	void update(Ogre::Real timeSinceLastFrame);

private:
	bool onQuit(const CEGUI::EventArgs &evt);
	bool onSinglePlayerSelected(const CEGUI::EventArgs &evt);

	CEGUI::Window *_sheet;
};

#endif

