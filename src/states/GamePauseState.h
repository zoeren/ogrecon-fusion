#ifndef _GamePauseState_h_
#define _GamePauseState_h_

#include "stdafx.h"

#include "states/GuiAppState.h"

class GamePauseState : public GuiAppState
{
public:
	GamePauseState();
	~GamePauseState();

	void createScene();

	void enter();
	void leave();

	bool keyPressed(const OIS::KeyEvent &evt);

	void update(Ogre::Real timeSinceLastFrame);

private:
	bool onResumeSelected(const CEGUI::EventArgs &evt = CEGUI::EventArgs());
	bool onMainMenuSelected(const CEGUI::EventArgs &evt);
	bool onQuitSelected(const CEGUI::EventArgs &evt);

	CEGUI::Window *_sheet;
};

#endif