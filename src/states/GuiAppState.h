#ifndef _GuiAppState_h_
#define _GuiAppState_h_

#include "stdafx.h"

#include "AppState.h"

class GuiAppState : public AppState
{
public:
	GuiAppState() { }
	virtual ~GuiAppState() { }
	
	virtual bool keyPressed(const OIS::KeyEvent &arg);
	virtual bool keyReleased(const OIS::KeyEvent &arg);

	virtual bool mouseMoved(const OIS::MouseEvent &arg);
	virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
	virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

protected:
	void createGuiScene(Ogre::String name);
	void destroyGuiScene();

private:
	CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);
};

#endif