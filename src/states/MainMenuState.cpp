#include "MainMenuState.h"


MainMenuState::MainMenuState() :
	_sheet(0)
{
}

MainMenuState::~MainMenuState()
{
}

void MainMenuState::createScene()
{
	OgreFramework::getSingletonPtr()->log->logMessage("MainMenu: Create Scene");
}

void MainMenuState::enter()
{
	OgreFramework::getSingletonPtr()->log->logMessage("MainMenu: Enter");
	
	createGuiScene("MainMenu");

	CEGUI::WindowManager &wmgr = CEGUI::WindowManager::getSingleton();
	_sheet = wmgr.createWindow("DefaultWindow", "MainMenuSheet");

	// main menu
	CEGUI::USize defaultBtnSize = CEGUI::USize(CEGUI::UDim(0.15f, 0.0f), CEGUI::UDim(0.05f, 0.0f));

	CEGUI::PushButton *singlePlayer = (CEGUI::PushButton*)wmgr.createWindow("TaharezLook/Button", "StartGameButton");
	singlePlayer->setText("Single Player");
	singlePlayer->setSize(defaultBtnSize);
	singlePlayer->setPosition(CEGUI::UVector2(CEGUI::UDim(0.425f, 0.0f), CEGUI::UDim(0.4f, 0.0f)));
	singlePlayer->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MainMenuState::onSinglePlayerSelected, this));

	CEGUI::PushButton *quit = (CEGUI::PushButton*)wmgr.createWindow("TaharezLook/Button", "QuitButton");
	quit->setText("Quit");
	quit->setSize(defaultBtnSize);
	quit->setPosition(CEGUI::UVector2(CEGUI::UDim(0.425f, 0.0f), CEGUI::UDim(0.475f, 0.0f)));
	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MainMenuState::onQuit, this));

	_sheet->addChild(singlePlayer);
	_sheet->addChild(quit);
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_sheet);
}

void MainMenuState::leave()
{
	OgreFramework::getSingletonPtr()->log->logMessage("MainMenu: Leave");

	destroyGuiScene();

	CEGUI::WindowManager::getSingleton().destroyWindow(_sheet);
}

bool MainMenuState::keyPressed(const OIS::KeyEvent &evt)
{
	if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_ESCAPE))
	{
		onQuit(CEGUI::EventArgs());
	}
	return GuiAppState::keyPressed(evt);
}

/*
bool MainMenuState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);
	return true;
}

bool MainMenuState::mouseMoved(const OIS::MouseEvent &evt)
{
	if(OgreFramework::getSingletonPtr()->mouseMoved(evt)) return true;
	return true;
}

bool MainMenuState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if(OgreFramework::getSingletonPtr()->mousePressed(evt, id)) return true;
	return true;
}

bool MainMenuState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if(OgreFramework::getSingletonPtr()->mouseReleased(evt, id)) return true;
	return true;
}
*/

/*
void MainMenuState::buttonHit(OgreBites::Button* button)
{
	if(button->getName() == "ExitBtn")
		_quit = true;
	else if(button->getName() == "EnterBtn")
		changeState("GameState");
}
*/

void MainMenuState::update(Ogre::Real timeSinceLastFrame)
{
	//_frameEvent.timeSinceLastFrame = timeSinceLastFrame;
	
	//OgreFramework::getSingletonPtr()->trayManager->frameRenderingQueued(_frameEvent);
}

bool MainMenuState::onQuit(const CEGUI::EventArgs &evt)
{
	shutdown();
	return true;
}

bool MainMenuState::onSinglePlayerSelected(const CEGUI::EventArgs &evt)
{
	this->changeState("Game");
	return true;
}
