#include "GameState.h"

GameState::GameState() :
	_paused(false),
	_game(0)
{
}

GameState::~GameState()
{
}

void GameState::enter()
{
	OgreFramework::getSingletonPtr()->log->logMessage("Game: Enter");
	
	_paused = false;

	_sceneManager = OgreFramework::getSingletonPtr()->root->createSceneManager(Ogre::ST_GENERIC, "GameSceneMgr");
	_sceneManager->addRenderQueueListener(OgreFramework::getSingletonPtr()->overlaySystem);

	_camera = _sceneManager->createCamera("GameCam");
	_camera->setPosition(Ogre::Vector3(0.0, 0.0, 130.0));
	_camera->lookAt(Ogre::Vector3(0, 0, 0));
	_camera->setNearClipDistance(1.0f);
	_camera->setAspectRatio(Ogre::Real(OgreFramework::getSingletonPtr()->viewport->getActualWidth()) / Ogre::Real(OgreFramework::getSingletonPtr()->viewport->getActualHeight()));

	OgreFramework::getSingletonPtr()->viewport->setCamera(_camera);

	_raySceneQuery = _sceneManager->createRayQuery(Ogre::Ray());

	_game = new Game();

	createScene();
}

void GameState::leave()
{
	OgreFramework::getSingletonPtr()->log->logMessage("Game: Leave");

	_sceneManager->destroyCamera(_camera);
	if(_sceneManager)
		OgreFramework::getSingletonPtr()->root->destroySceneManager(_sceneManager);

	_game->destroy();
}

void GameState::pause()
{
	OgreFramework::getSingletonPtr()->log->logMessage("Game: Pause");

	_paused = true;
}

void GameState::resume()
{
	OgreFramework::getSingletonPtr()->log->logMessage("Game: Resume");

	_paused = false;
}

bool GameState::keyPressed(const OIS::KeyEvent &evt)
{
	if (evt.key == OIS::KC_ESCAPE)
	{
		pushState("GamePause");
	}
	else if(evt.key == OIS::KC_D)
	{
		OgreFramework *ogre = OgreFramework::getSingletonPtr();
		if(ogre->debugOverlay->isVisible())
			ogre->debugOverlay->hide();
		else
			ogre->debugOverlay->show();
	}
	return true;
}

void GameState::update(Ogre::Real timeSinceLastFrame)
{
	if (!_paused)
	{
		_game->update(timeSinceLastFrame);
	}
}

void GameState::createScene()
{
	// Create a Light and set its position
	Ogre::Light* light = _sceneManager->createLight("MainLight");
	light->setPosition(_camera->getPosition());
	light->setDiffuseColour(Ogre::ColourValue(0.9f, 0.9f, 0.9f));

	_game->create(_sceneManager);

	// Create an Entity
	//Ogre::Entity* planet = _sceneManager->createEntity("Planet", "sphere.mesh");

	// Create a SceneNode and attach the Entity to it
//	_planet = _sceneManager->getRootSceneNode()->createChildSceneNode("PlanetNode");
//	headNode->showBoundingBox(true);
//	_planet->attachObject((Ogre::MovableObject*)planet);
//	_planet->scale(0.1f, 0.1f, 0.1f);
}

bool GameState::mouseMoved(const OIS::MouseEvent &arg)
{
	if (!_paused)
	{
		/*
		CEGUI::Vector2f mousePos = CEGUI::System::getSingletonPtr()->getDefaultGUIContext().getMouseCursor().getPosition();
		Ogre::Ray mouseRay = _camera->getCameraToViewportRay(mousePos.d_x / float(arg.state.width), mousePos.d_y / float(arg.state.height));

		_raySceneQuery->setRay(mouseRay);
		Ogre::RaySceneQueryResult& result = _raySceneQuery->execute();
		
		Ogre::StringStream str;
		str << "Results: " << result.size() << "\n";
		OgreFramework::getSingletonPtr()->log->logMessage(str.str());
		*/

		/*
		Ogre::RaySceneQueryResult::iterator iter;
		//check to see if the mouse is pointing at the world and put our current object at that location
		for (iter= result.begin(); iter != result.end(); iter++)
		{
			iter->movable->getParentSceneNode()->scale(0.2, 0.2, 0.2);
		}
		*/
		if (arg.state.Z.rel)
		{
			float scroll = arg.state.Z.rel / 120.0f;
			_camera->moveRelative(Ogre::Vector3(0.0, 0.0, -scroll * 1.5f));
		}
	}
	return GuiAppState::mouseMoved(arg);
}