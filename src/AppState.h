#ifndef _AppState_h_
#define _AppState_h_

#include "stdafx.h"

#include "OgreFramework.h"

class AppState;

class AppStateListener
{
public:
	AppStateListener() { }
	virtual ~AppStateListener() { }

	virtual void registerState(AppState* state, Ogre::String stateName) = 0;
	virtual AppState* findByName(Ogre::String stateName) = 0;
	virtual void changeState(AppState *state) = 0;
	virtual bool pushState(AppState* state) = 0;
	virtual void popState() = 0;
	virtual void popAllAndPushState(AppState* state) = 0;
	virtual void pauseState() = 0;
	virtual void shutdown() = 0;
};

/**
 * An abstract state of the application.
 * Contains a Camera, SceneManager and FrameEvent(?).
 */
class AppState abstract : public OIS::KeyListener, 
						  public OIS::MouseListener
{
	friend class AppStateManager;

public:
	AppState(AppStateListener* parent) { initialise(parent); }
	AppState() { initialise(); }
	virtual ~AppState() { }

	virtual void enter() = 0;
	virtual void leave() = 0;
	virtual void pause() { }
	virtual void resume() { }
	
	void destroy() { delete this; }
	void changeState(Ogre::String stateName) { _parent->changeState(_parent->findByName(stateName)); }
	void pushState(Ogre::String stateName) { _parent->pushState(_parent->findByName(stateName)); }
	void popState() { _parent->popState(); }
	void popAllAndPushState(Ogre::String stateName) { _parent->popAllAndPushState(_parent->findByName(stateName)); }
	void shutdown() { _parent->shutdown(); }

	virtual void update(Ogre::Real timeSinceLastFrame) = 0;

protected:
	AppStateListener* _parent;
	Ogre::Camera* _camera;
	Ogre::SceneManager*	_sceneManager;
   // Ogre::FrameEvent _frameEvent;

private:
	void initialise(AppStateListener* parent = 0)
	{
		_parent = parent;
		_camera = 0;
		_sceneManager = 0;
		//_frameEvent = Ogre::FrameEvent();
	}
};

#endif