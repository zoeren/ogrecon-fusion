#include "Application.h"

Application::Application() :
	_stateManager(0)
{

}

Application::~Application()
{
	delete _stateManager;
	delete OgreFramework::getSingletonPtr();
}

void Application::run(AppStatesMap states, AppState* initialState)
{
	if (!OgreFramework::start("Ogrecon-Fusion"))
		return;

	_stateManager = new AppStateManager;

	for (AppStatesMap::iterator it = states.begin(); it != states.end(); it++)
	{
		_stateManager->registerState(it->second, it->first);
	}

	_stateManager->start(initialState);
}