#ifndef _AppStateManager_h_
#define _AppStateManager_h_

#include "stdafx.h"

#include "OgreFramework.h"
#include "AppState.h"

class AppStateManager : public AppStateListener
{
public:
	AppStateManager();
	~AppStateManager();

	void start(AppState* state);

	// from AppStateListener
	void registerState(AppState* state, Ogre::String stateName);
	AppState* findByName(Ogre::String stateName);
	void changeState(AppState *state);
	bool pushState(AppState* state);
	void popState();
	void popAllAndPushState(AppState* state);
	void pauseState();
	void shutdown();
    
private:
	void initialiseState(AppState* state);

	std::vector<AppState*> _activeStates;
	std::map<Ogre::String, AppState*> _registeredStates;
	bool _shutdown;
};

#endif