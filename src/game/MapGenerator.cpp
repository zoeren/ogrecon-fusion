#include "MapGenerator.h"

MapGenerator::MapGenerator(void)
{
}

MapGenerator::~MapGenerator(void)
{
}

std::vector<Planet*> MapGenerator::create(MapProperties properties)
{
	std::vector<Planet*> map;
	int planetId = 1;

	float x0 = -properties.width / 2;
	float y0 = -properties.height / 2;

	// two players
	for (int i = 1; i <= 2; i++)
	{
		Planet *homePlanet = new Planet(planetId++);
		float dx = (properties.maxPlanetSize / 2) + (properties.width / 8);
		float dy = (properties.maxPlanetSize / 2) + (properties.height / 8);
		float x, y;
		switch (i)
		{
		case 1:
			x = x0 + dx;
			y = y0 + dy;
			break;
		case 2:
			x = x0 + properties.width - dx;
			y = y0 + properties.height - dy;
			break;
		}
		homePlanet->position = Ogre::Vector3(x, y, 0);
		homePlanet->owner = i;
		homePlanet->size = properties.maxPlanetSize;
		map.push_back(homePlanet);
	}

	return map;
}
