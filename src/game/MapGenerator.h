#ifndef _MapGenerator_h_
#define _MapGenerator_h_

#include "stdafx.h"

#include "game/Planet.h"

struct MapProperties
{
	float width;
	float height;
	float maxPlanetSize;
	int numPlanets;
};

class MapGenerator
{
public:
	MapGenerator();
	~MapGenerator();

	/************************************************************************/
	/* Generates a map of planets. Only generates maps for two players.     */
	/************************************************************************/
	std::vector<Planet*> create(MapProperties properties);
};

#endif
