#ifndef _Game_h_
#define _Game_h_

#include "stdafx.h"

#include <vector>

#include "game/Planet.h"
#include "game/MapGenerator.h"

class Game
{
public:
	Game();
	~Game();

	void update(Ogre::Real timeSinceLastFrame);
	void destroy();
	void create(Ogre::SceneManager *sceneManager);

private:
	std::vector<Planet*> _planets;
};

#endif