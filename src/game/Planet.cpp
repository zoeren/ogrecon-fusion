#include "Planet.h"


Planet::Planet(int id) :
	_id(id),
	_sceneNode(0)
{

}

Planet::~Planet()
{
}

void Planet::create(Ogre::String modelName, Ogre::SceneManager *sceneManager)
{
	Ogre::StringStream sstr;
	sstr << "Planet-" << this->_id << "\n";
	Ogre::Entity *entity = sceneManager->createEntity(sstr.str(), modelName);
	_sceneNode = sceneManager->getRootSceneNode()->createChildSceneNode(sstr.str()+"-Node");
	_sceneNode->attachObject((Ogre::MovableObject*) entity);
	_sceneNode->setPosition(this->position);
	_sceneNode->scale(this->size, this->size, this->size);

	//Ogre::MovableObject* msg = new Ogre::MovableObject("TXT_001", "this is the caption");
	//msg->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_ABOVE); // Center horizontally and display above the node
	/* msg->setAdditionalHeight( 2.0f ); //msg->setAdditionalHeight( ei.getRadius() ) // apparently not needed from 1.7*/
	//_sceneNode->attachObject(msg);
}

int Planet::getId()
{
	return _id;
}
