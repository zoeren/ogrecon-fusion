#include "Game.h"


Game::Game()
{
}

Game::~Game()
{
}

void Game::create(Ogre::SceneManager *sceneManager)
{
	MapGenerator mapGen;
	MapProperties props;
	props.height = 100.0f;
	props.width = 150.0f;
	props.numPlanets = 10;
	props.maxPlanetSize = 7.5f;
	_planets = mapGen.create(props);

	for (std::vector<Planet*>::iterator it = _planets.begin(); it != _planets.end(); it++)
	{
		(*it)->create("mars.mesh", sceneManager);
	}
}

void Game::update(Ogre::Real timeSinceLastFrame)
{
	
}

void Game::destroy()
{
	while (!_planets.empty())
	{
		delete _planets.back();
		_planets.pop_back();
	}
}
