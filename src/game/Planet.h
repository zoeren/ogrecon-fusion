#ifndef _Planet_h_
#define _Planet_h_

#include "stdafx.h"
#include "game/Player.h"

class Planet
{
public:
	Planet(int id);
	~Planet();

	void create(Ogre::String modelName, Ogre::SceneManager *sceneManager);
	int getId();

	Ogre::Vector3 position;
	PlayerId owner;
	float size;

private:
	int _id;
	Ogre::SceneNode *_sceneNode;
};

#endif
