#ifndef _Player_h_
#define _Player_h_

#include "stdafx.h"

typedef int PlayerId;

class Player
{
public:
	Player(PlayerId id) : _id(id) { }
	virtual ~Player() { }

	PlayerId getId() { return _id; }

private:
	PlayerId _id;
};

#endif