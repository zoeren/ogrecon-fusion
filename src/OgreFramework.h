#ifndef _OgreFramework_h_
#define _OgreFramework_h_

#include "stdafx.h"

class OgreFramework : public Ogre::Singleton<OgreFramework>, 
					  public Ogre::FrameListener,
					  OIS::KeyListener, 
					  OIS::MouseListener
{
public:
	OgreFramework();
	~OgreFramework();

	static bool start(Ogre::String windowTitle);

	bool initialise(Ogre::String wndTitle);
	void update(Ogre::Real timeSinceLastFrame);

	// from KeyListener
	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	// from MouseListener
	bool mouseMoved(const OIS::MouseEvent &evt);
	bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

	// from FrameListener
	bool frameRenderingQueued(const Ogre::FrameEvent& evt);

	Ogre::Root *root;
	Ogre::RenderWindow *renderWindow;
	Ogre::OverlaySystem *overlaySystem;
	Ogre::Viewport *viewport;
	Ogre::Timer *timer;
	Ogre::Log *log;

	OIS::InputManager *inputManager;
	OIS::Keyboard *keyboard;
	OIS::Mouse *mouse;

	CEGUI::OgreRenderer *guiRenderer;
	CEGUI::System *guiSystem;

	Ogre::Overlay *debugOverlay;
	
private:
	OgreFramework(const OgreFramework&);
	OgreFramework& operator= (const OgreFramework&);

	void initialiseResources();
	void initialiseInput();
	void initialiseUserInterface();
	
	void initialiseDebugStats();
	void updateStats();

	Ogre::String _configResources;
	Ogre::String _configPlugins;
};

#endif
